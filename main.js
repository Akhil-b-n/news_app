const key = import.meta.env.VITE_key2
const mainnews = document.getElementById('mainnews')
const subnews = document.getElementById('subnews')

async function fetchdata(){
try {
     
  const response = await fetch(`https://gnews.io/api/v4/search?q=headlines&lang=en&country=in&max=4&apikey=${key}`);  
  const data = await response.json();
  return data
  //console.log(response)
  //console.log(data)
  // get the data from api
  //console.log(articles);
  // get the first article
  //const article = articles[0];
  //console.log(article);
}
catch(error) {
  console.log('Error fetching data:', error);
};
}

fetchdata() 
 .then(data =>{
  const mainnews = document.getElementById('mainnews')
  const subnews = document.getElementById('subnews')

  const articles = data.articles;
  console.log(articles)
  
  //loop begins
  for(let i=0; i<4; i++){
    const article = articles[i]
    
    //main news
    if(i===0){
     //link
     const link = document.createElement('a')
     link.href= article.url;
     mainnews.appendChild(link)
     //div for arrangement
     const div1 =document.createElement('div')
     div1.classList.add("flex")
     div1.classList.add("flex-col")
     div1.classList.add("justify-between")
     div1.classList.add("items-center")
     div1.classList.add("gap-3")
     link.appendChild(div1)
     //image
     const image = document.createElement('img')
     image.src=article.image
     image.classList.add("w-full")
     div1.appendChild(image)
     //title
     const title = document.createElement('p')
     title.classList.add("text-lg")
     title.classList.add("font-semibold")
     title.classList.add("py-4")
     title.textContent=article.title
     div1.appendChild(title)
     //description
     const desc = document.createElement('span')
     desc.classList.add("text-sm")
     desc.textContent=article.description
     div1.appendChild(desc) 
    }

    else{
    //link
    console.log(article)
    const slink = document.createElement('a')
    slink.href= article.url;
    subnews.appendChild(slink)
    //div for arrangement
    const div2 =document.createElement('div')
    div2.classList.add("flex")
    div2.classList.add("flex-row")
    div2.classList.add("justify-between")
    div2.classList.add("items-center")
    div2.classList.add("gap-6")
    slink.appendChild(div2)
    //image
    const image1 = document.createElement('img')
    image1.src=article.image
    image1.classList.add("w-1/4")
    div2.appendChild(image1)
    //div for arrangement
    const div3 =document.createElement('div')
    div3.classList.add("flex")
    div3.classList.add("flex-col")
    div3.classList.add("justify-between")
    div3.classList.add("items-center")
    div3.classList.add("gap-3")
    div2.appendChild(div3)
    //title
    const title1 = document.createElement('p')
    title1.classList.add("text-md")
    title1.classList.add("font-semibold")
    title1.classList.add("py-4")
    title1.textContent=article.title
    div3.appendChild(title1)
    //description
    const descr = document.createElement('span')
    descr.classList.add("text-sm")
    descr.textContent=article.description
    div3.appendChild(descr)
    }

  }
 
})
/*
//sports api fetch
try {
     
  const response = await fetch(`https://gnews.io/api/v4/search?q=sports&lang=en&country=in&max=2&apikey=${key}`);  
  const data = await response.json();
   // get the data from api
  const articles = data.articles;
  console.log(articles);
  // get the first article
  const article = articles[0];
  console.log(article);
  slink.href= article.url;
  stitle.textContent= article.title;
  scontent.textContent=article.description;
  simag.src=article.image
  
  const article1 = articles[1];
  console.log(article1);
  mlink.href=article.url;
  mtitle.textContent= article1.title;
  mcontent.textContent=article1.description;
  mimag.src=article1.image
}
catch(error) {
  console.log('Error fetching data:', error);
};


//politics api fetch
try {
     
  const response = await fetch(`https://gnews.io/api/v4/search?q=politics&lang=en&country=in&max=1&apikey=${key}`);  
  const data = await response.json();
   // get the data from api
  const articles = data.articles;
  console.log(articles);
  // get the first article
  const article = articles[0];
  console.log(article);
  plink.href= article.url;
  ptitle.textContent= article.title;
  pcontent.textContent=article.description;
  pimag.src=article.image

}
catch(error) {
  console.log('Error fetching data:', error);
};

//movies api fetch
/*try {
     
  const response = await fetch(`https://gnews.io/api/v4/search?q=business&lang=en&country=in&max=2&apikey=key`);  
  const data = await response.json();
   // get the data from api
  const articles = data.articles;
  console.log(data)
  console.log(articles);
  // get the first article
  const article = articles[1];
  console.log(article);
  mlink.href= article.url;
  mtitle.textContent= article.title;
  mcontent.textContent=article.description;
  mimag.src=article.image

}
catch(error) {
  console.log('Error fetching data:', error);
};*/